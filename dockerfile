FROM python:alpine
COPY entrypoint.sh /opt/
COPY minedash_app /opt/minedash_app
COPY etc/conf.py /etc/minedash/
RUN apk add --no-cache \
      bash \
      gcc \
      curl \
      g++ \
      libstdc++ \
      linux-headers \
      musl-dev \
      postgresql-dev \
      mariadb-dev \
      jpeg-dev \
      zlib-dev \
      libmagic && \
    pip install --upgrade pip && \
    pip install -r /opt/minedash_app/requirements.txt && \
    pip install gunicorn && \
    apk del \
          gcc \
          g++ \
          linux-headers \
          musl-dev;
HEALTHCHECK --interval=2s --timeout=5s --retries=5 \
   CMD curl -f http://localhost:8000/ || exit 1
EXPOSE 8000
WORKDIR /opt
ENTRYPOINT ["/opt/entrypoint.sh"]
CMD ["--start-service"]
