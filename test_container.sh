for i in {1..20}
do
    health=$(docker inspect -f '{{.State.Health.Status}}' minedash-site)
    echo "container health is: $health"
    if [ $health == "healthy" ]
    then
        echo "Container test passed."
        exit 0
    fi
    echo "Failed attempt $i"
    sleep 2
done
echo "Container test failed all attempts."
exit 1
