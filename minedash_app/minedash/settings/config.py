from __future__ import absolute_import, unicode_literals
import os

from minedash.utils import parse_boolean, ensure_config

from .base import * # noqa

# The hostnames that Django is allowed to serve from.
# https://docs.djangoproject.com/en/1.11/ref/settings/#allowed-hosts
#
# DJANGO_ALLOWED_HOSTS: a comma separated list.
#
# SECURITY WARNING: Default of '*' is unsafe for production.

_allowed_hosts = os.environ.get('DJANGO_ALLOWED_HOSTS')
if _allowed_hosts:
    ALLOWED_HOSTS = _allowed_hosts.split(',')
else:
    ALLOWED_HOSTS = ['*']

# Controls if Djangos debug mode is turned on.
# https://docs.djangoproject.com/en/1.11/ref/settings/#debug
#
# DJANGO_DEBUG: a boolean like string
#
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = parse_boolean(os.environ.get('DJANGO_DEBUG', True))

# A key used by Django for cryptographic signing of sessions, and
# token generation.
# https://docs.djangoproject.com/en/1.11/ref/settings/#secret-key
#
# DJANGO_SECRET_KEY: a long unpredictable string value
#
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get(
    'DJANGO_SECRET_KEY',
    "This is a terrible secret key. Don't use me in production."
)


# Database configuration
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
#
# DJANGO_DATABASE_ENGINE: module name for engine
# DJANGO_DATABASE_NAME: name of the db or path to db file if sqlite3
# DJANGO_DATABASE_USER: name of db user
# DJANGO_DATABASE_PASSWORD: password for db user
# DJANGO_DATABASE_HOST: host address for db, default is localhost
# DJANGO_DATABASE_PORT: port for db, defaults to default port for db type
_db_engine = os.environ.get(
    'DJANGO_DATABASE_ENGINE', 'django.db.backends.sqlite3')
if _db_engine == 'django.db.backends.sqlite3':
    DATABASES = {
        'default': {
            'ENGINE': _db_engine,
            'NAME': os.environ.get(
                'DJANGO_DATABASE_NAME',
                os.path.join(BASE_DIR, 'db.sqlite3')), # noqa
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': _db_engine,
            'NAME': os.environ.get('DJANGO_DATABASE_NAME', ''),
            'USER': os.environ.get('DJANGO_DATABASE_USER', ''),
            'PASSWORD': os.environ.get('DJANGO_DATABASE_PASSWORD', ''),
            'HOST': os.environ.get('DJANGO_DATABASE_HOST', ''),
            'PORT': os.environ.get('DJANGO_DATABASE_PORT', ''),
        }
    }

# Logging settings
# https://docs.djangoproject.com/en/1.11/ref/settings/#logging
#
# These are setup to go to console for ease of use with containers.
# Override if needed with `local.py`.
#
# DJANGO_LOGGING_LEVEL: one of DEBUG, INFO, WARNING, ERROR, CRITICAL
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'minedash': {
            'format':
                'DJANGO: %(levelname)s %(asctime)s %(pathname)s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': os.environ.get('DJANGO_LOGGING_LEVEL', 'INFO'),
            'class': 'logging.StreamHandler',
            'formatter': 'minedash',
        },
    },
    'loggers': {
        'django': {
            'handlers': [
                'console',
            ],
            'level': os.environ.get('DJANGO_LOGGING_LEVEL', 'INFO'),
            'propagate': False,
        }
    }
}

# Email settings
#
# EMAIL_BACKEND: module of email backend
# https://docs.djangoproject.com/en/1.11/ref/settings/#email-backend
#
# smtp backend settings:
# EMAIL_HOST: host address for smtp server
# EMAIL_HOST_PASSWORD: email user password
# EMAIL_HOST_USER: email user name
# EMAIL_PORT: smtp server port
# EMAIL_USE_TLS: boolean like, use explict TLS\
# https://docs.djangoproject.com/en/1.11/ref/settings/#email-use-tls
# EMAIL_USE_SSL: boolean like, use implict TLS
# https://docs.djangoproject.com/en/1.11/ref/settings/#email-use-tls
#
# file backend settings:
# EMAIL_FILE_PATH: location of email files
EMAIL_BACKEND = os.environ.get(
    'DJANGO_EMAIL_BACKEND', 'django.core.mail.backends.console.EmailBackend')
if EMAIL_BACKEND == 'django.core.mail.backends.smtp.EmailBackend':
    EMAIL_HOST = os.environ.get('DJANGO_EMAIL_HOST', 'localhost')
    EMAIL_HOST_PASSWORD = os.environ.get('DJANGO_EMAIL_HOST_PASSWORD', '')
    EMAIL_HOST_USER = os.environ.get('DJANGO_EMAIL_HOST_USER', '')
    EMAIL_PORT = int(os.environ.get('DJANGO_EMAIL_PORT', 25))
    EMAIL_USE_TLS = parse_boolean(
        os.environ.get('DJANGO_EMAIL_USE_TLS', False))
    EMAIL_USE_SSL = parse_boolean(
        os.environ.get('DJANGO_EMAIL_USE_SSL', False))
    DEFAULT_FROM_EMAIL = os.environ.get(
        'DJANGO_DEFAULT_FROM_EMAIL', 'wagtail@localhost')
elif EMAIL_BACKEND == 'django.core.mail.backends.filebased.EmailBackend':
    EMAIL_FILE_PATH = os.environ.get(
        'DJANGO_EMAIL_FILE_PATH', '/tmp/django-messages')

# Timezone used for datetime when presenting datetimes to users
# https://docs.djangoproject.com/en/1.11/ref/settings/#std:setting-TIME_ZONE
#
# DJANGO_TIME_ZONE: A name of valid timezone.
TIME_ZONE = os.environ.get('DJANGO_TIME_ZONE', 'Pacific/Auckland')


# Date and Datetime formats
# https://docs.djangoproject.com/en/1.11/ref/settings/#date-format
# https://docs.djangoproject.com/en/1.11/ref/settings/#datetime-format
#
# DATE_FORMAT, DATETIME_FORMAT: a valid date or datetime filter string
# https://docs.djangoproject.com/en/1.11/ref/templates/builtins/#std:templatefilter-date
DATE_FORMAT = os.environ.get('DJANGO_DATE_FORMAT', 'j F, Y')
DATETIME_FORMAT = os.environ.get('DJANGO_DATETIME_FORMAT', 'j F, Y, P')


_STATIC_ROOT = os.environ.get("DJANGO_STATIC_ROOT", None)
if _STATIC_ROOT:
    STATIC_ROOT = _STATIC_ROOT

_MEDIA_ROOT = os.environ.get("DJANGO_MEDIA_ROOT", None)
if _MEDIA_ROOT:
    MEDIA_ROOT = _MEDIA_ROOT

# File and Static storage settings:
# https://docs.djangoproject.com/en/1.11/ref/settings/#default-file-storage
# https://docs.djangoproject.com/en/1.11/ref/settings/#staticfiles-storage
# https://django-compressor.readthedocs.io/en/latest/remote-storages/
DEFAULT_FILE_STORAGE = os.environ.get(
    'DJANGO_DEFAULT_FILE_STORAGE',
    'django.core.files.storage.FileSystemStorage')

STATICFILES_STORAGE = os.environ.get(
    'DJANGO_STATICFILES_STORAGE',
    'django.contrib.staticfiles.storage.StaticFilesStorage')

COMPRESS_STORAGE = os.environ.get(
    'DJANGO_COMPRESS_STORAGE',
    'compressor.storage.CompressorFileStorage')


# If using swift storage, we have a lot of required settings
# https://github.com/dennisv/django-storage-swift

if (DEFAULT_FILE_STORAGE == 'swift.storage.SwiftStorage' or
        (STATICFILES_STORAGE ==
            'minedash.storage.CachedStaticSwiftStorage') or
        COMPRESS_STORAGE == 'minedash.storage.CachedStaticSwiftStorage'):
    pass
    # DJANGO_SWIFT_USERNAME: username to use to authenticate.
    # DJANGO_SWIFT_PASSWORD: password to use to authenticate.
    # DJANGO_SWIFT_AUTH_URL: The URL for the auth server (keystone)
    # DJANGO_SWIFT_PROJECT_NAME: project name to use when authenticating
    # DJANGO_SWIFT_USER_DOMAIN_NAME: domain name we authenticate to
    #   Defaults to: "Default"
    # DJANGO_SWIFT_PROJECT_DOMAIN_NAME: domain name our project is located in
    #   Defaults to: "Default"
    # DJANGO_SWIFT_REGION_NAME: OpenStack region name eg. nz~hlz~1
    # DJANGO_SWIFT_CONTAINER_NAME: container in which to store the files
    #   Defaults to: "django_storage"
    # DJANGO_SWIFT_STATIC_CONTAINER_NAME: container for storing staticfiles
    #   Defaults to: "django_static"
    # DJANGO_SWIFT_AUTO_CREATE_CONTAINER: boolean -
    #     Should the container be created if it does not exist?
    #   Defaults to: True
    # DJANGO_SWIFT_AUTO_CREATE_CONTAINER_PUBLIC: boolean -
    #     Set the auto created container as public on creation
    #   Defaults to: True
    # DJANGO_SWIFT_AUTO_CREATE_CONTAINER_ALLOW_ORIGIN:
    #     Set the container's X-Container-Meta-Access-Control-Allow-Origin
    #     value, to support CORS requests.
    #   Defaults to: "*"

    _err_msg = "All swift storage config values are required."
    SWIFT_USERNAME = ensure_config(
        os.environ.get('DJANGO_SWIFT_USERNAME'), err_msg=_err_msg)
    SWIFT_PASSWORD = ensure_config(
        os.environ.get('DJANGO_SWIFT_PASSWORD'), err_msg=_err_msg)
    SWIFT_AUTH_URL = ensure_config(
        os.environ.get('DJANGO_SWIFT_AUTH_URL'), err_msg=_err_msg)
    SWIFT_PROJECT_NAME = ensure_config(
        os.environ.get('DJANGO_SWIFT_PROJECT_NAME'), err_msg=_err_msg)
    SWIFT_USER_DOMAIN_NAME = os.environ.get(
        'DJANGO_SWIFT_USER_DOMAIN_NAME', 'Default')
    SWIFT_PROJECT_DOMAIN_NAME = os.environ.get(
        'DJANGO_SWIFT_PROJECT_DOMAIN_NAME', 'Default')
    SWIFT_REGION_NAME = ensure_config(
        os.environ.get('DJANGO_SWIFT_REGION_NAME'), err_msg=_err_msg)
    SWIFT_CONTAINER_NAME = os.environ.get(
        'DJANGO_SWIFT_CONTAINER_NAME', 'django_storage')
    SWIFT_STATIC_CONTAINER_NAME = os.environ.get(
        'DJANGO_SWIFT_STATIC_CONTAINER_NAME', 'django_static')
    SWIFT_AUTO_CREATE_CONTAINER = parse_boolean(os.environ.get(
        'DJANGO_SWIFT_AUTO_CREATE_CONTAINER', True))
    SWIFT_AUTO_CREATE_CONTAINER_PUBLIC = parse_boolean(os.environ.get(
        'DJANGO_SWIFT_AUTO_CREATE_CONTAINER_PUBLIC', True))
    SWIFT_AUTO_CREATE_CONTAINER_ALLOW_ORIGIN = os.environ.get(
        'DJANGO_SWIFT_AUTO_CREATE_CONTAINER_ALLOW_ORIGIN', '*')

    COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssRelativeFilter']


# human-readable name of your Wagtail install
# http://docs.wagtail.io/en/v1.13.1/advanced_topics/settings.html#site-name
WAGTAIL_SITE_NAME = os.environ.get(
    'DJANGO_WAGTAIL_SITE_NAME', 'MineDash')

# Base URL to use when referring to full URLs within the Wagtail admin backend
# e.g. in notification emails. Don't include '/admin' or a trailing slash
BASE_URL = os.environ.get(
    'DJANGO_BASE_URL', 'http://localhost:8000')


# Analytics settings
GOOGLE_ANALYTICS_TRACKING_ID = os.environ.get(
    'DJANGO_GOOGLE_ANALYTICS_TRACKING_ID', "")


# Robots.txt settings:
ROBOTS_CACHE_TIMEOUT = int(os.environ.get(
    'DJANGO_ROBOTS_CACHE_TIMEOUT', 60*60))

ROBOTS_DISALLOW_ALL = parse_boolean(os.environ.get(
    'DJANGO_ROBOTS_DISALLOW_ALL', True))


# Recaptcha settings
# https://github.com/praekelt/django-recaptcha
#
# Used for form pages (contact form).
# If empty will simply not use recaptcha.
RECAPTCHA_PUBLIC_KEY = os.environ.get('DJANGO_RECAPTCHA_PUBLIC_KEY', "")
RECAPTCHA_PRIVATE_KEY = os.environ.get('DJANGO_RECAPTCHA_PRIVATE_KEY', "")


# Minecraft settings:
MINECRAFT_SERVER_URL = os.environ.get('MINECRAFT_SERVER_URL', '')
MINECRAFT_RCON_PASSWORD = os.environ.get('MINECRAFT_RCON_PASSWORD', '')
MINECRAFT_RCON_PORT = int(os.environ.get('MINECRAFT_RCON_PORT', 25575))
MINECRAFT_RCON_TLSMODE = int(os.environ.get('MINECRAFT_RCON_TLSMODE', 0))
