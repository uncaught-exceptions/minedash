from django.core.files.storage import get_storage_class
from django.conf import settings

from swift.storage import StaticSwiftStorage


class CachedStaticSwiftStorage(StaticSwiftStorage):
    """
    S3 storage backend that saves the files locally for compression.
    """
    def __init__(self, *args, **kwargs):
        super(StaticSwiftStorage, self).__init__(*args, **kwargs)
        self.local_storage = get_storage_class(
            "compressor.storage.CompressorFileStorage")()

    def save(self, name, content):
        self.local_storage._save(name, content)

        if not name.endswith(tuple(settings.ONLY_LOCAL_STATIC_FILE_TYPES)):
            super(CachedStaticSwiftStorage, self).save(
                name, self.local_storage._open(name))
        return name
