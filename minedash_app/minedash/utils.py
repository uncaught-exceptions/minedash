from minedash.exceptions import MissingConfigurationException


def parse_boolean(value):
    if type(value) == bool:
        return value

    if value in ['True', 'true', 'T', 't', '1']:
        return True
    if value in ['False', 'false', 'F', 'f', '0']:
        return False
    return False


def ensure_config(value, err_msg=None):
    if value is None:
        raise MissingConfigurationException(message=err_msg)
    return value
